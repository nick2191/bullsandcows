package com.company;

import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Pattern;

public class BullsAndCows {

    public int level;
    public int step;
    public int[] question;
    public int[] answer;
    private static final int MIN_LEVEL = 3;
    private static final int MAX_LEVEL = 5;

    private class BullsAndCowsDto {
        int bulls = 0;
        int cows = 0;
    }

    public void run() {
        System.out.println("Для выхода введите exit...");
        try {
            setGameLevel();
            runGame();
            congratulations();
        } catch (Exception ex) {
            //Выход из игры через выброс исключения
            System.out.print(ex.getLocalizedMessage());
        }
    }

    private void setGameLevel() throws Exception {
        do {
            level = getInt("Введите количество цифр, которые загадает компьютер (3, 4, 5): ");
        }
        while (!(level >= MIN_LEVEL && level <= MAX_LEVEL));
        question = new int[level];
        setQuestionArray();
    }

    private void runGame() throws Exception {
        while (true) {
            do {
                answer = new int[level];
            }
            while (!userStep());
            var bullsCowsDto = new BullsAndCowsDto();
            computerStep(bullsCowsDto);
            printResult(bullsCowsDto);
            if (bullsCowsDto.bulls == level) break;
            step++;
        }
    }

    private void congratulations() {
        System.out.println("Поздравляю! За " + step + " ходов вы выиграли!");
        for (var q : question) {
            System.out.print(q);
        }
    }

    private void printResult(BullsAndCowsDto bullsCowsDto) {
        System.out.println("Быки: " + bullsCowsDto.bulls + " Коровы: " + bullsCowsDto.cows + " Ход: " + step + "\n");
    }

    //Ввод с клавиатуры
    private static String getString(Pattern regex, String message) throws Exception {
        String result = "";
        Scanner scan = new Scanner(System.in);
        while (!result.matches(String.valueOf(regex))) {
            System.out.print(message);
            result = scan.nextLine();
            if (result.equals("exit")) {
                throw new Exception("Game over!");
            }
        }
        return result;
    }

    //Ввод числа с клавиатуры
    private static int getInt(String message) throws Exception {
        Pattern regex = Pattern.compile("\\d+");
        return Integer.parseInt(getString(regex, message));
    }

    //Ввод числа с клавиатуры и проверка количества введенных знаков
    private static int getIntAndCompare(String message, int digit) throws Exception {
        Pattern regex = Pattern.compile("\\d+");
        var result = getString(regex, message);
        if (result.length() != digit) return 0;
        return Integer.parseInt(result);
    }

    private void setQuestionArray() {
        HashSet<Integer> hashSet = new HashSet<Integer>();
        for (int i = 0; i < question.length; i++) {
            while (true) {
                int random = (int) (Math.random() * 10);
                if (hashSet.add(random)) {
                    question[i] = random;
                    break;
                }
            }
        }
    }

    private boolean userStep() throws Exception {
        int result = 0;
        int digits = answer.length;
        HashSet<Integer> hashSet = new HashSet<Integer>();
        while (result == 0) {
            result = Math.abs(getIntAndCompare("Ваш ход, введите число c неповторяющимися цифрами: ", digits));
        }
        for (int i = digits - 1; i >= 0; i--) {
            int exp = (int) Math.pow(10, i);
            //Массив получается "перевернутый"
            int reverseIndex = digits - i - 1;
            answer[reverseIndex] = result / exp;
            result -= answer[reverseIndex] * exp;
            if (!hashSet.add(answer[reverseIndex])) {
                return false;
            }
        }
        return true;
    }

    //Неоптимально, О(n2), можно существенно улучшить
    private void computerStep(BullsAndCowsDto bullsCowsDto) {
        for (var questionIndex = 0; questionIndex < question.length; questionIndex++) {
            for (var answerIndex = 0; answerIndex < answer.length; answerIndex++) {
                if (answer[answerIndex] == question[questionIndex]) {
                    if (answerIndex == questionIndex) {
                        bullsCowsDto.bulls++;
                    } else {
                        bullsCowsDto.cows++;
                    }
                }
            }
        }
    }
}